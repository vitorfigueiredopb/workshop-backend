from django.views.generic import ListView, CreateView, DeleteView
from django.urls import reverse_lazy
from .models import Pessoa  # importa o pacote pessoa de models
from .forms import PessoaForm

# Funcao da pagina inicial
def home(request):
    return render(request, 'pessoas/home.html')

# Classe para listar todas as pessoas do banco de dados.
class PessoaListView(ListView): # Cria view generica
    model =  Pessoa
    template_name = 'pessoas/listar_pessoas.html'
    content_type_name = 'pessoas'

# Classe para criar e enviar dados para o banco de dados.
class PessoaCreateView(CreateView):
    model = Pessoa
    template_name = 'pessoas/criar_pessoa.html'
    form_class = PessoaForm
    success_url = reverse_lazy('pessoas_pessoa_list')

class PessoaDeleteView(DeleteView):
    model = Pessoa
    success_url = reverse_lazy('pessoas_pessoa_list')