from django.urls import path
from .views import PessoaListView, PessoaCreateView, PessoaDeleteView

urlpatterns = [
    path('', PessoaListView.as_view(), name='pessoas_pessoa_list'),
    path('criar/', PessoaCreateView.as_view(), name='pessoas_pessoa_create'),
    path('<int:pk>/delete', PessoaDeleteView.as_view(), name='pessoas_pessoa_delete')
]